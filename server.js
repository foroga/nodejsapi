var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
var path = require('path');
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname,"index.html"));
});

app.post('/', function(req,res) {
  res.send("Hemos recibido su petición")
});

app.get('/clientes', function(req, res){
  res.sendFile(path.join(__dirname,"clienteConsulta.json"));
});

app.post('/clientes/:idcliente', function(req, res){
  res.send("Alta del cliente " + req.params.idcliente);
});

app.put('/clientes/:idcliente', function(req, res){
  res.send("Modificación del cliente " + req.params.idcliente);
});

app.delete('/clientes/:idcliente', function(req, res){
  res.send("Baja del cliente " + req.params.idcliente);
});
